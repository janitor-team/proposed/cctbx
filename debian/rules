#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
# export DH_VERBOSE=1

# This has to be exported to make some magic below work.
export DH_OPTIONS

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

export PYBUILD_SYSTEM=custom

# parallel support
NPROC ?= $(shell dh_assistant which-build-system| jq .parallel)

# Multiarch
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

builddir        = build
BUILDDIR        = $(CURDIR)/$(builddir)

# dxtbx needs dials to be packaged.
MODULES         = cctbx cbflib scitbx crys3d libtbx iotbx wxtbx smtbx

%:
	dh $@ --with python3 --buildsystem=pybuild

override_dh_auto_build:
	python3 setup.py build
	cd $(BUILDDIR) && python3 ../libtbx/configure.py --build=debug --use_environment_flags $(MODULES)
	cd $(BUILDDIR) && ./bin/libtbx.scons -j $(NPROC)
	iconv -f ISO-8859-1 -t UTF-8 cctbx/eltbx/covalent_radii.h > covalent_radii.h
	mv covalent_radii.h cctbx/eltbx/covalent_radii.h

override_dh_auto_test:
	python3 setup.py test

override_dh_auto_clean:
	dh_auto_clean
	$(RM) -r $(BUILDDIR)
	find . -name __pycache__ -type d -exec rm -rf {} \;

